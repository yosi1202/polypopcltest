﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ParametersPanel : MonoBehaviour
{
    [SerializeField] TMP_InputField scaleSpeed;
    [SerializeField] TMP_InputField horizontalSpeed;
    [SerializeField] TMP_InputField startSpeed;
    [SerializeField] TMP_InputField hitShrink;
    [SerializeField] TMP_InputField acceleration;
    [SerializeField] TMP_InputField hitInterval;
    [SerializeField] TMP_InputField gameTime;
    [SerializeField] TMP_InputField cameraHight;
    [SerializeField] TMP_InputField cameraZ;
    [SerializeField] TMP_InputField cameraAngle;

    /*void Start()
    {
        Parameters.instance.scaleSpeed = float.Parse(scaleSpeed.text);
        Parameters.instance.horizontalSpeed = float.Parse(horizontalSpeed.text);
        Parameters.instance.startSpeed = float.Parse(startSpeed.text);
        Parameters.instance.hitShrink = float.Parse(hitShrink.text);
        Parameters.instance.acceleration = float.Parse(acceleration.text);
        Parameters.instance.hitInterval = float.Parse(hitInterval.text);
        Parameters.instance.gameTime = float.Parse(gameTime.text);
        Parameters.instance.cameraHight = float.Parse(cameraHight.text);
        Parameters.instance.cameraZ = float.Parse(cameraZ.text);
        Parameters.instance.cameraAngle = float.Parse(cameraAngle.text);
    }*/

    public void ParametersChange()
    {
        Parameters.instance.scaleSpeed = float.Parse(scaleSpeed.text);
        Parameters.instance.horizontalSpeed = float.Parse(horizontalSpeed.text);
        Parameters.instance.startSpeed = float.Parse(startSpeed.text);
        Parameters.instance.hitShrink = float.Parse(hitShrink.text);
        Parameters.instance.acceleration = float.Parse(acceleration.text);
        Parameters.instance.hitInterval = float.Parse(hitInterval.text);
        Parameters.instance.gameTime = float.Parse(gameTime.text);
        Parameters.instance.cameraHight = float.Parse(cameraHight.text);
        Parameters.instance.cameraZ = float.Parse(cameraZ.text);
        Parameters.instance.cameraAngle = float.Parse(cameraAngle.text);
        Time.timeScale = 1;
    }

    public void StartGame()
    {
        ParametersChange();
        SceneManager.LoadScene("Game");
    }

    void OnEnable()
    {
        Time.timeScale = 0;
        scaleSpeed.text = Parameters.instance.scaleSpeed.ToString();
        horizontalSpeed.text = Parameters.instance.horizontalSpeed.ToString();
        startSpeed.text = Parameters.instance.startSpeed.ToString();
        hitShrink.text = Parameters.instance.hitShrink.ToString();
        acceleration.text = Parameters.instance.acceleration.ToString();
        hitInterval.text = Parameters.instance.hitInterval.ToString();
        gameTime.text = Parameters.instance.gameTime.ToString();
        cameraHight.text = Parameters.instance.cameraHight.ToString();
        cameraZ.text = Parameters.instance.cameraZ.ToString();
        cameraAngle.text = Parameters.instance.cameraAngle.ToString();
    }
}
