﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class SnowBallController : MonoBehaviour
{
    /*[SerializeField] float scaleSpeed;
    [SerializeField] float horspeed;
    [SerializeField] float speed;
    [SerializeField] float hitShrink;

    [SerializeField] float acceleration;

    [SerializeField] float hitInterval;*/

    [SerializeField] Transform ball;
    [SerializeField] ParticleSystem expolotion;
    [SerializeField] Transform collectables;
    [SerializeField] Camera cameraObj;
    [SerializeField] Animator anim;
    [SerializeField] Rigidbody rb;
    [SerializeField] AudioSource AS;

    [SerializeField] GameObject fracturedBall;
    [SerializeField] GameObject fracturedExplution;

    [SerializeField] Transform lastGround;
    [SerializeField] GameObject GroundRamp;

    [SerializeField] Spawner spawner;

    [SerializeField] float gameTime = 60;
    float starttime;
    float hittime;
    bool levelend = false;

    float speed;

    // Start is called before the first frame update
    void Start()
    {
        starttime = Time.time;
        speed = Parameters.instance.startSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (levelend)
            return;

        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Ray ray = cameraObj.ScreenPointToRay(touch.position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, hit.point.x, Parameters.instance.horizontalSpeed * Time.deltaTime), transform.position.y, transform.position.z);
            }
        }

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed * Time.deltaTime);
    }
    void FixedUpdate()
    {
        if (levelend)
            return;

        ball.localScale += Vector3.one*Parameters.instance.scaleSpeed;
        ball.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        collectables.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        expolotion.transform.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        expolotion.startSpeed += Parameters.instance.scaleSpeed;
        expolotion.startSize += Parameters.instance.scaleSpeed;
        cameraObj.transform.localPosition = new Vector3(0, 0, cameraObj.transform.localPosition.z - Parameters.instance.scaleSpeed*3);

        speed += Parameters.instance.acceleration;
        anim.speed += Parameters.instance.acceleration/20;
    }

    void OnTriggerEnter(Collider other)
    {
        if (levelend)
            return;
        if(other.tag == "EndGrund")
        {
            if (Time.time - starttime < gameTime)
            {
                other.transform.parent.position = lastGround.GetChild(0).position;
                other.transform.parent.localScale += Vector3.one *0.3f;
                lastGround = other.transform.parent;
            }
            else
            {
                //Instantiate(GroundRamp, lastGround.GetChild(0).position, Quaternion.identity);
                spawner.StopAll();
                GroundRamp.transform.position = lastGround.GetChild(0).position;
                GroundRamp.SetActive(true);
            }
        }
        if(other.tag == "Collectable")
        {
            other.enabled = false;
            Runner runner = other.GetComponent<Runner>();
            if (runner != null)
                runner.Collect();
            other.transform.parent = transform.GetChild(0);

            ball.localScale += Vector3.one*Parameters.instance.scaleSpeed*50;
            ball.localPosition = new Vector3(0, ball.localScale.y/2, 0);
            collectables.localPosition = new Vector3(0, ball.localScale.y/2, 0);
            expolotion.transform.localPosition = new Vector3(0, ball.localScale.y/2, 0);
            expolotion.startSpeed += Parameters.instance.scaleSpeed*50;
            expolotion.startSize += Parameters.instance.scaleSpeed*50;
            cameraObj.transform.localPosition = new Vector3(0, 0, cameraObj.transform.localPosition.z - Parameters.instance.scaleSpeed*3);
        }
        if(other.tag == "Obstacle")
        {
            if (Time.time - hittime > Parameters.instance.hitInterval)
            {
                hittime = Time.time;
                CameraShaker.Instance.ShakeOnce (5, 5, 0.5f, 0.5f);
                expolotion.Play();
                BallSrink();
                AS.Play();
            }
        }

        if(other.tag == "EndLevel")
        {
            if (Time.time - hittime > Parameters.instance.hitInterval)
            {
                spawner.RemoveAll();
                levelend = true;
                rb.isKinematic = false;
                rb.useGravity = true;
                rb.AddForce(Vector3.up*650);
            }
        }
    }

    void BallSrink()
    {
        ball.localScale -= Vector3.one*Parameters.instance.hitShrink;
        ball.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        collectables.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        expolotion.transform.localPosition = new Vector3(0, ball.localScale.y/2, 0);
        expolotion.startSpeed -= Parameters.instance.hitShrink;
        expolotion.startSize -= Parameters.instance.hitShrink;
        cameraObj.transform.localPosition = new Vector3(0, 0, cameraObj.transform.localPosition.z + Parameters.instance.hitShrink*3);
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        Debug.Log(collisionInfo.collider.tag);
        //if (collisionInfo.collider.tag == "Ground")
        //{
            //expolotion.Play();
            //CameraShaker.Instance.ShakeOnce (5, 5, 0.5f, 0.5f);
            AS.Play();
            ball.gameObject.SetActive(false);
            collectables.gameObject.SetActive(false);
            fracturedBall.transform.parent = transform;
            fracturedBall.SetActive(true);
            fracturedExplution.transform.parent = transform;
            ExplosionSource exs = fracturedExplution.GetComponent<ExplosionSource>();
            exs.PosStart = fracturedBall.transform.position;
            exs.PosEnd = fracturedBall.transform.position;
            fracturedExplution.SetActive(true);
            rb.isKinematic =true;
            rb.useGravity =false;
        //}
    }
}
