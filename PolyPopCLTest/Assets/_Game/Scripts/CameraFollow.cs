﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform ball;
    /*[SerializeField] float CameraHight;
    [SerializeField] float CameraZ;
    [SerializeField] float CameraAngle;*/

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(0, ball.position.y + Parameters.instance.cameraHight, ball.position.z + Parameters.instance.cameraZ);
        transform.eulerAngles = new Vector3(Parameters.instance.cameraAngle, 0, 0);
    }
}
