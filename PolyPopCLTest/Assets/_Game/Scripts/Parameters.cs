﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parameters : MonoBehaviour
{
    public static Parameters instance;

    public float scaleSpeed;
    public float horizontalSpeed;
    public float startSpeed;
    public float hitShrink;
    public float acceleration;
    public float hitInterval;
    public float gameTime;
    public float cameraHight;
    public float cameraZ;
    public float cameraAngle;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;

        DontDestroyOnLoad(gameObject);
        Application.targetFrameRate = 40;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
