﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject[] Obstacles;
    [SerializeField] GameObject[] Collectables;
    [SerializeField] Transform ball;

    [SerializeField] Transform parentobj;

    [SerializeField] float spawntimeObstacles;
    [SerializeField] float spawntimeCollectables;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObstacle", 0, spawntimeObstacles);
        InvokeRepeating("SpawnObstacle", 0.4f, spawntimeObstacles);
        InvokeRepeating("SpawnCollectables", 0.5f, spawntimeCollectables);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnObstacle()
    {
        Instantiate(Obstacles[Random.Range(0, Obstacles.Length)], 
                    new Vector3(Random.Range(-ball.localScale.x *2, ball.localScale.x*2), 0, ball.position.z + 40 + ball.localScale.x*10), 
                    Quaternion.Euler(-50, 0,0), parentobj);
    }

    void SpawnCollectables()
    {
        GameObject randCollectable = Collectables[Random.Range(0, Collectables.Length)];
        Instantiate(randCollectable, 
                    new Vector3(Random.Range(-ball.localScale.x *2, ball.localScale.x*2), 0, ball.position.z + 40 + ball.localScale.x*10), 
                    Quaternion.Euler(randCollectable.transform.eulerAngles.x, randCollectable.transform.eulerAngles.y,0), parentobj);
    }

    public void StopAll()
    {
        CancelInvoke();
    }

    public void RemoveAll()
    {
        parentobj.gameObject.SetActive(false);
    }
}
