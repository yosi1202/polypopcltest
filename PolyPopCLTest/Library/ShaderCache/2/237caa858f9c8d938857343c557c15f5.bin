<Q                         LIGHTPROBE_SH      UNITY_HDR_ON    >I  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _DetailAlbedoMap_ST;
uniform 	mediump float _UVSec;
uniform 	vec2 _V_CW_MainTex_Scroll;
uniform 	vec2 _V_CW_DetailTex_Scroll;
uniform 	vec4 _V_CW_PivotPoint_Position;
uniform 	vec4 _V_CW_PivotPoint_2_Position;
uniform 	vec2 _V_CW_Angle;
uniform 	vec2 _V_CW_MinimalRadius;
in highp vec4 in_POSITION0;
in mediump vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
in highp vec2 in_TEXCOORD1;
in mediump vec4 in_TANGENT0;
out highp vec4 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD4;
out mediump vec4 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
bvec4 u_xlatb1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
vec4 u_xlat8;
bvec2 u_xlatb8;
vec3 u_xlat9;
vec4 u_xlat10;
vec4 u_xlat11;
bvec3 u_xlatb11;
vec4 u_xlat12;
vec3 u_xlat13;
vec4 u_xlat14;
vec4 u_xlat15;
bvec3 u_xlatb16;
mediump float u_xlat16_17;
mediump vec3 u_xlat16_18;
vec3 u_xlat20;
vec3 u_xlat25;
vec2 u_xlat28;
vec2 u_xlat29;
vec2 u_xlat40;
vec2 u_xlat42;
float u_xlat57;
bool u_xlatb57;
void main()
{
    u_xlat0.y = 0.0;
    u_xlat20.y = _V_CW_PivotPoint_2_Position.x;
    u_xlat2.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat2.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat2.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat57 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat57 = inversesqrt(u_xlat57);
    u_xlat2.xyz = vec3(u_xlat57) * u_xlat2.xyz;
    u_xlat3.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].yzx;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].yzx * in_TANGENT0.xxx + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].yzx * in_TANGENT0.zzz + u_xlat3.xyz;
    u_xlat57 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat57 = inversesqrt(u_xlat57);
    u_xlat4.xyz = vec3(u_xlat57) * u_xlat3.xyz;
    u_xlat5.xyz = u_xlat2.xyz * u_xlat4.zxy;
    u_xlat2.xyz = u_xlat2.zxy * u_xlat4.xyz + (-u_xlat5.xyz);
    u_xlat4.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].yzx;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].yzx * in_POSITION0.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].yzx * in_POSITION0.zzz + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].yzx * in_POSITION0.www + u_xlat4.xyz;
    u_xlat2.xyz = (-u_xlat2.xyz) + u_xlat4.yzx;
    u_xlat5.z = u_xlat2.y;
    u_xlat3.xyz = u_xlat3.xyz * vec3(u_xlat57) + u_xlat4.xyz;
    u_xlat5.y = u_xlat3.z;
    u_xlat5.x = u_xlat4.z;
    u_xlat6.xyz = (-u_xlat5.xyz) + _V_CW_PivotPoint_2_Position.xxx;
    u_xlat5.xyz = (-u_xlat5.xyz) + _V_CW_PivotPoint_Position.xxx;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_V_CW_PivotPoint_Position.y<0.0);
#else
    u_xlatb57 = _V_CW_PivotPoint_Position.y<0.0;
#endif
    u_xlat7.x = (u_xlatb57) ? -1.0 : 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_V_CW_PivotPoint_2_Position.y<0.0);
#else
    u_xlatb57 = _V_CW_PivotPoint_2_Position.y<0.0;
#endif
    u_xlat7.y = (u_xlatb57) ? -1.0 : 1.0;
    u_xlat1.xw = u_xlat7.xy * vec2(_V_CW_MinimalRadius.x, _V_CW_MinimalRadius.y);
    u_xlat7.x = _V_CW_PivotPoint_Position.y;
    u_xlat7.yz = _V_CW_PivotPoint_2_Position.yz;
    u_xlatb8.xy = lessThan(abs(u_xlat7.xyxx), vec4(_V_CW_MinimalRadius.x, _V_CW_MinimalRadius.y, _V_CW_MinimalRadius.x, _V_CW_MinimalRadius.x)).xy;
    u_xlat8.x = (u_xlatb8.x) ? u_xlat1.x : u_xlat7.x;
    u_xlat8.y = (u_xlatb8.y) ? u_xlat1.w : u_xlat7.y;
    u_xlatb1.xw = lessThan(u_xlat8.xxxy, vec4(0.0, 0.0, 0.0, 0.0)).xw;
    u_xlat1.x = (u_xlatb1.x) ? float(-1.0) : float(1.0);
    u_xlat1.w = (u_xlatb1.w) ? float(-1.0) : float(1.0);
    u_xlat1.xw = u_xlat1.xw * _V_CW_Angle.xy;
    u_xlat7.xy = u_xlat8.xy * u_xlat1.xw;
    u_xlat9.xy = u_xlat7.xy * vec2(0.0174532942, 0.0174532942);
    u_xlat6.xyz = abs(u_xlat6.xyz) / u_xlat9.yyy;
    u_xlat10.xyz = u_xlat6.xyz * vec3(1.57079637, 1.57079637, 1.57079637);
    u_xlat10.xyz = cos(u_xlat10.xyz);
    u_xlat10.xyz = (-u_xlat10.xyz) * u_xlat10.xyz + vec3(1.0, 1.0, 1.0);
    u_xlat20.x = _V_CW_PivotPoint_2_Position.z * u_xlat10.x + u_xlat4.y;
    u_xlatb11.xyz = lessThan(u_xlat6.xyzx, vec4(1.0, 1.0, 1.0, 0.0)).xyz;
    u_xlat6.xyz = u_xlat6.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlat6.xyz = min(max(u_xlat6.xyz, 0.0), 1.0);
#else
    u_xlat6.xyz = clamp(u_xlat6.xyz, 0.0, 1.0);
#endif
    u_xlat6.xyz = u_xlat1.www * u_xlat6.xyz;
    u_xlat6.xyz = u_xlat6.xyz * vec3(0.00872664619, 0.00872664619, 0.00872664619);
    u_xlat7.w = (-u_xlat9.y);
    u_xlat7.xy = u_xlat4.yz + u_xlat7.zw;
    u_xlat12.yz = (u_xlatb11.x) ? u_xlat20.xy : u_xlat7.xy;
    u_xlat13.z = u_xlat8.y;
    u_xlat12.x = u_xlat4.x;
    u_xlat13.xy = _V_CW_PivotPoint_2_Position.zx;
    u_xlat20.xyz = u_xlat12.xyz + (-u_xlat13.zxy);
    u_xlat7.x = cos(u_xlat6.x);
    u_xlat6.x = sin(u_xlat6.x);
    u_xlat14 = u_xlat6.xxxx * vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat15 = u_xlat20.zxyz * u_xlat14.wzww;
    u_xlat15 = u_xlat14.wwzw * u_xlat20.xyzx + (-u_xlat15);
    u_xlat15 = u_xlat20.yzxy * u_xlat7.xxxx + u_xlat15;
    u_xlat6.xw = u_xlat14.zw * u_xlat15.zw;
    u_xlat6.xw = u_xlat14.yx * u_xlat15.yx + (-u_xlat6.wx);
    u_xlat0.xz = u_xlat6.xw * vec2(2.0, 2.0);
    u_xlat0.xyz = u_xlat0.xyz + u_xlat20.xyz;
    u_xlat0.xyz = u_xlat13.zxy + u_xlat0.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_V_CW_PivotPoint_2_Position.x<u_xlat4.z);
#else
    u_xlatb57 = _V_CW_PivotPoint_2_Position.x<u_xlat4.z;
#endif
    u_xlat0.xyz = (bool(u_xlatb57)) ? u_xlat0.xyz : u_xlat4.xyz;
    u_xlat14.y = 0.0;
    u_xlat20.y = _V_CW_PivotPoint_Position.x;
    u_xlat9.z = _V_CW_PivotPoint_Position.z;
    u_xlat4.xw = u_xlat4.yz + u_xlat9.zx;
    u_xlat5.xyz = abs(u_xlat5.xyz) / u_xlat9.xxx;
    u_xlat15.xyz = u_xlat5.xyz * vec3(1.57079637, 1.57079637, 1.57079637);
    u_xlat15.xyz = cos(u_xlat15.xyz);
    u_xlat15.xyz = (-u_xlat15.xyz) * u_xlat15.xyz + vec3(1.0, 1.0, 1.0);
    u_xlat20.x = _V_CW_PivotPoint_Position.z * u_xlat15.x + u_xlat4.y;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(u_xlat4.z<_V_CW_PivotPoint_Position.x);
#else
    u_xlatb57 = u_xlat4.z<_V_CW_PivotPoint_Position.x;
#endif
    u_xlatb16.xyz = lessThan(u_xlat5.xyzx, vec4(1.0, 1.0, 1.0, 0.0)).xyz;
    u_xlat5.xyz = u_xlat5.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.xyz = min(max(u_xlat5.xyz, 0.0), 1.0);
#else
    u_xlat5.xyz = clamp(u_xlat5.xyz, 0.0, 1.0);
#endif
    u_xlat5.xyz = u_xlat1.xxx * u_xlat5.xyz;
    u_xlat5.xyz = u_xlat5.xyz * vec3(0.00872664619, 0.00872664619, 0.00872664619);
    u_xlat12.yz = (u_xlatb16.x) ? u_xlat20.xy : u_xlat4.xw;
    u_xlat8.zw = _V_CW_PivotPoint_Position.xz;
    u_xlat1.xyz = (-u_xlat8.xwz) + u_xlat12.xyz;
    u_xlat4.x = sin(u_xlat5.x);
    u_xlat5.x = cos(u_xlat5.x);
    u_xlat4 = u_xlat4.xxxx * vec4(0.0, -1.0, -1.0, 0.0);
    u_xlat12 = u_xlat1.zxyz * u_xlat4.wzww;
    u_xlat12 = u_xlat4.wwzw * u_xlat1.xyzx + (-u_xlat12);
    u_xlat12 = u_xlat1.yzxy * u_xlat5.xxxx + u_xlat12;
    u_xlat42.xy = u_xlat4.zw * u_xlat12.zw;
    u_xlat4.xy = u_xlat4.yx * u_xlat12.yx + (-u_xlat42.yx);
    u_xlat14.xz = u_xlat4.xy * vec2(2.0, 2.0);
    u_xlat1.xyz = u_xlat1.xyz + u_xlat14.xyz;
    u_xlat1.xyz = u_xlat8.xwz + u_xlat1.xyz;
    u_xlat0.xyz = (bool(u_xlatb57)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat1 = u_xlat0.xxxx * hlslcc_mtx4x4unity_WorldToObject[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToObject[0] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToObject[2] * u_xlat0.yyyy + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToObject[3];
    u_xlat4 = u_xlat1.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat4 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat1.xxxx + u_xlat4;
    u_xlat4 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat1.zzzz + u_xlat4;
    u_xlat4 = u_xlat4 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat12 = u_xlat4.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat12 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat4.xxxx + u_xlat12;
    u_xlat12 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat4.zzzz + u_xlat12;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat4.wwww + u_xlat12;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_UVSec==0.0);
#else
    u_xlatb57 = _UVSec==0.0;
#endif
    u_xlat4.xy = (bool(u_xlatb57)) ? in_TEXCOORD0.xy : in_TEXCOORD1.xy;
    u_xlat4.xy = u_xlat4.xy * _DetailAlbedoMap_ST.xy + _DetailAlbedoMap_ST.zw;
    vs_TEXCOORD0.zw = vec2(_V_CW_DetailTex_Scroll.x, _V_CW_DetailTex_Scroll.y) * _Time.xx + u_xlat4.xy;
    u_xlat4.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD0.xy = _V_CW_MainTex_Scroll.xy * _Time.xx + u_xlat4.xy;
    u_xlat4.xyz = u_xlat1.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat1.xxx + u_xlat4.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat1.zzz + u_xlat4.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * u_xlat1.www + u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz + (-_WorldSpaceCameraPos.xyz);
    vs_TEXCOORD6.xyz = u_xlat1.xyz;
    vs_TEXCOORD2 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD3 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1.z = u_xlat2.z;
    u_xlat4.xy = u_xlat2.xy + u_xlat7.zw;
    u_xlat42.xy = u_xlat3.yz + u_xlat7.zw;
    u_xlat7.x = _V_CW_PivotPoint_2_Position.z * u_xlat10.z + u_xlat2.x;
    u_xlat29.x = _V_CW_PivotPoint_2_Position.z * u_xlat10.y + u_xlat3.y;
    u_xlat7.y = _V_CW_PivotPoint_2_Position.x;
    u_xlat1.xy = (u_xlatb11.z) ? u_xlat7.xy : u_xlat4.xy;
    u_xlat7.xyz = (-u_xlat13.xyz) + u_xlat1.xyz;
    u_xlat4.x = sin(u_xlat6.z);
    u_xlat5.x = cos(u_xlat6.z);
    u_xlat10.x = cos(u_xlat6.y);
    u_xlat6.x = sin(u_xlat6.y);
    u_xlat12 = u_xlat4.xxxx * vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat14 = u_xlat7.yzxy * u_xlat12.wzww;
    u_xlat14 = u_xlat12.wwzw * u_xlat7.zxyz + (-u_xlat14);
    u_xlat14 = u_xlat7.xyzx * u_xlat5.xxxx + u_xlat14;
    u_xlat4.xy = u_xlat12.zw * u_xlat14.zw;
    u_xlat4.xy = u_xlat12.xy * u_xlat14.xy + (-u_xlat4.xy);
    u_xlat12.yz = u_xlat4.xy * vec2(2.0, 2.0);
    u_xlat12.x = 0.0;
    u_xlat25.xyz = u_xlat7.xyz + u_xlat12.xyz;
    u_xlat25.xyz = u_xlat13.xyz + u_xlat25.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_V_CW_PivotPoint_2_Position.x<u_xlat2.y);
#else
    u_xlatb57 = _V_CW_PivotPoint_2_Position.x<u_xlat2.y;
#endif
    u_xlat25.xyz = (bool(u_xlatb57)) ? u_xlat25.xyz : u_xlat2.xyz;
    u_xlat40.xy = u_xlat2.xy + u_xlat9.zx;
    u_xlat4.xy = u_xlat3.yz + u_xlat9.zx;
    u_xlat7.x = _V_CW_PivotPoint_Position.z * u_xlat15.z + u_xlat2.x;
    u_xlat28.x = _V_CW_PivotPoint_Position.z * u_xlat15.y + u_xlat3.y;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(u_xlat2.y<_V_CW_PivotPoint_Position.x);
#else
    u_xlatb57 = u_xlat2.y<_V_CW_PivotPoint_Position.x;
#endif
    u_xlat7.y = _V_CW_PivotPoint_Position.x;
    u_xlat1.xy = (u_xlatb16.z) ? u_xlat7.xy : u_xlat40.xy;
    u_xlat1.xyz = (-u_xlat8.wzx) + u_xlat1.xyz;
    u_xlat2.x = sin(u_xlat5.z);
    u_xlat5.x = cos(u_xlat5.z);
    u_xlat7.x = sin(u_xlat5.y);
    u_xlat9.x = cos(u_xlat5.y);
    u_xlat2 = u_xlat2.xxxx * vec4(0.0, -1.0, -1.0, 0.0);
    u_xlat12 = u_xlat1.yzxy * u_xlat2.wzww;
    u_xlat12 = u_xlat2.wwzw * u_xlat1.zxyz + (-u_xlat12);
    u_xlat5 = u_xlat1.xyzx * u_xlat5.xxxx + u_xlat12;
    u_xlat40.xy = u_xlat2.zw * u_xlat5.zw;
    u_xlat2.xy = u_xlat2.xy * u_xlat5.xy + (-u_xlat40.xy);
    u_xlat2.yz = u_xlat2.xy * vec2(2.0, 2.0);
    u_xlat2.x = 0.0;
    u_xlat1.xyz = u_xlat1.xyz + u_xlat2.xyz;
    u_xlat1.xyz = u_xlat8.wzx + u_xlat1.xyz;
    u_xlat1.xyz = (bool(u_xlatb57)) ? u_xlat1.xyz : u_xlat25.xyz;
    u_xlat1.xyz = (-u_xlat0.yzx) + u_xlat1.xyz;
    u_xlat29.y = _V_CW_PivotPoint_2_Position.x;
    u_xlat2.yz = (u_xlatb11.y) ? u_xlat29.xy : u_xlat42.xy;
    u_xlat2.x = u_xlat3.x;
    u_xlat5.xyz = (-u_xlat13.zxy) + u_xlat2.xyz;
    u_xlat6 = u_xlat6.xxxx * vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat11 = u_xlat5.zxyz * u_xlat6.wzww;
    u_xlat11 = u_xlat6.wwzw * u_xlat5.xyzx + (-u_xlat11);
    u_xlat10 = u_xlat5.yzxy * u_xlat10.xxxx + u_xlat11;
    u_xlat42.xy = u_xlat6.zw * u_xlat10.zw;
    u_xlat42.xy = u_xlat6.yx * u_xlat10.yx + (-u_xlat42.yx);
    u_xlat6.xz = u_xlat42.xy * vec2(2.0, 2.0);
    u_xlat6.y = 0.0;
    u_xlat5.xyz = u_xlat5.xyz + u_xlat6.xyz;
    u_xlat5.xyz = u_xlat13.zxy + u_xlat5.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(_V_CW_PivotPoint_2_Position.x<u_xlat3.z);
#else
    u_xlatb57 = _V_CW_PivotPoint_2_Position.x<u_xlat3.z;
#endif
    u_xlat3.xyw = (bool(u_xlatb57)) ? u_xlat5.xyz : u_xlat3.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb57 = !!(u_xlat3.z<_V_CW_PivotPoint_Position.x);
#else
    u_xlatb57 = u_xlat3.z<_V_CW_PivotPoint_Position.x;
#endif
    u_xlat28.y = _V_CW_PivotPoint_Position.x;
    u_xlat2.yz = (u_xlatb16.y) ? u_xlat28.xy : u_xlat4.xy;
    u_xlat2.xyz = (-u_xlat8.xwz) + u_xlat2.xyz;
    u_xlat4 = u_xlat7.xxxx * vec4(0.0, -1.0, -1.0, 0.0);
    u_xlat5 = u_xlat2.zxyz * u_xlat4.wzww;
    u_xlat5 = u_xlat4.wwzw * u_xlat2.xyzx + (-u_xlat5);
    u_xlat5 = u_xlat2.yzxy * u_xlat9.xxxx + u_xlat5;
    u_xlat42.xy = u_xlat4.zw * u_xlat5.zw;
    u_xlat4.xy = u_xlat4.yx * u_xlat5.yx + (-u_xlat42.yx);
    u_xlat4.xz = u_xlat4.xy * vec2(2.0, 2.0);
    u_xlat4.y = 0.0;
    u_xlat2.xyz = u_xlat2.xyz + u_xlat4.xyz;
    u_xlat2.xyz = u_xlat8.xwz + u_xlat2.xyz;
    u_xlat2.xyz = (bool(u_xlatb57)) ? u_xlat2.xyz : u_xlat3.xyw;
    u_xlat0.xyz = (-u_xlat0.xyz) + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat1.zxy * u_xlat0.yzx + (-u_xlat2.xyz);
    u_xlat57 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat57 = inversesqrt(u_xlat57);
    u_xlat0.xyz = vec3(u_xlat57) * u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_WorldToObject[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat57 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat57 = inversesqrt(u_xlat57);
    u_xlat0.xyz = vec3(u_xlat57) * u_xlat0.xyz;
    u_xlat1.x = dot(u_xlat0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(u_xlat0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(u_xlat0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat1.xyz;
    vs_TEXCOORD4.xyz = u_xlat0.xyz;
    vs_TEXCOORD4.w = 0.0;
    u_xlat16_17 = u_xlat0.y * u_xlat0.y;
    u_xlat16_17 = u_xlat0.x * u_xlat0.x + (-u_xlat16_17);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_18.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_18.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_18.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_17) + u_xlat16_18.xyz;
    vs_TEXCOORD5.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 _Color;
uniform 	mediump float _Metallic;
uniform 	float _Glossiness;
uniform 	mediump float _OcclusionStrength;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _OcclusionMap;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD4;
in mediump vec4 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_6;
void main()
{
    u_xlat16_0.x = texture(_OcclusionMap, vs_TEXCOORD0.xy).y;
    u_xlat16_1 = (-_OcclusionStrength) + 1.0;
    u_xlat16_0.w = u_xlat16_0.x * _OcclusionStrength + u_xlat16_1;
    u_xlat16_1 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat16_2.xyz * _Color.xyz;
    u_xlat16_6.xyz = _Color.xyz * u_xlat16_2.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = vec3(u_xlat16_1) * u_xlat16_3.xyz;
    SV_Target0 = u_xlat16_0;
    SV_Target1.w = _Glossiness;
    u_xlat2.x = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat1.xyz = u_xlat2.xxx * vs_TEXCOORD4.xyz;
    u_xlat2.xyz = u_xlat1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    SV_Target2.xyz = u_xlat2.xyz;
    SV_Target2.w = 1.0;
    u_xlat1.w = 1.0;
    u_xlat16_4.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_4.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_4.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_4.xyz = u_xlat16_4.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_4.xyz = max(u_xlat16_4.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_2.xyz = log2(u_xlat16_4.xyz);
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_2.xyz = exp2(u_xlat16_2.xyz);
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_2.xyz = max(u_xlat16_2.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_4.xyz = u_xlat16_0.www * u_xlat16_2.xyz;
    SV_Target3.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    SV_Target3.w = 1.0;
    return;
}

#endif
  7                             $GlobalsL      
   unity_SHAr                        
   unity_SHAg                       
   unity_SHAb                           _Color                    0   	   _Metallic                     @      _Glossiness                   D      _OcclusionStrength                    H          $Globals�        _Time                            _WorldSpaceCameraPos                     
   unity_SHBr                        
   unity_SHBg                    0   
   unity_SHBb                    @   	   unity_SHC                     P      _MainTex_ST                         _DetailAlbedoMap_ST                   0     _UVSec                    @     _V_CW_MainTex_Scroll                  H     _V_CW_DetailTex_Scroll                    P     _V_CW_PivotPoint_Position                     `     _V_CW_PivotPoint_2_Position                   p     _V_CW_Angle                   �     _V_CW_MinimalRadius                   �     unity_ObjectToWorld                  `      unity_WorldToObject                  �      unity_MatrixVP                   �             _MainTex                  _OcclusionMap                